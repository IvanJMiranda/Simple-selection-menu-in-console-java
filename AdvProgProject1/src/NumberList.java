/**
 * For now, this class has a method to calculate the average of n values given their SUM, as I can't use ArrayList for this project yet...
 * With time, it may change to a class containing more methods that make various operations with a list of numbers and even constructors
 * to create objects that represent a list.
 * @author Ivan J. Miranda Marrero
 *
 */
public class NumberList
{
	/**
	 * This static method calculates the average of a list of numbers given their sum.
	 * @param sumOfValues the sum of the values to be averaged.
	 * @param valuesCount the number of values added together.
	 * @return the average of the list of numbers if there were at least one. If no numbers were supplied, the method returns -1.
	 */
	public static double getAverage(double sumOfValues, int valuesCount)
	{
		if (valuesCount > 0)
		{
			double avg = sumOfValues/valuesCount;
			return avg;
		}
		else
			return -1;
		
	} //End method.
	
} //End class.
