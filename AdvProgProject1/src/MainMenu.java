import java.util.Scanner; //This import is for reading input.
/**
 * This is the main class of the advanced programming project 1. It runs the main menu.
 * @author Ivan J. Miranda Marrero
 *
 */

public class MainMenu //Main class contains main method.
{
	public static void main(String[] args) //Main method controls program execution.
	{
		//Input reader initialization. Input is not stored directly in a variable. Rather, it is used in the comparisons directly.
		@SuppressWarnings("resource")
		Scanner userInput = new Scanner(System.in);	
		int quadraticTaskCounter = 0; //Counter for quadratic equation solving task. //Purpose of this counters is to make them visible
		int averageTaskCounter = 0; //Counter for average of a list of numbers task. //to the user in usage statistics.
		int stringReverseTaskCounter = 0; //Counter for the reversing of a string task.
		int stringFormatTaskCounter = 0; //Counter for the formatting of a string task.
		int useStatisticsTaskCounter = 0; //Counter for the task of showing use statistics.
		final int INITIAL_TRIES = 3;
		int tries = INITIAL_TRIES; //Tries counter. Used to count user's tries.
		//TODO create boolean finished to control return option instead of just assigning 0 to tries. (Consider not repeating options and making an object of type menu)
				
		//Welcome.
		System.out.println("\nWelcome! This program lets you do from the following options.\nDeveloped by Ivan J. Miranda.");
		
		do //While loop controls selection, program execution and tries left.
		{
			//Main menu and options.
			System.out.println("\nMain Menu. Please select from the following options by entering its number:\n");
			System.out.println("1. Perform operations on numbers.");
			System.out.println("2. Perform operations on strings.");
			System.out.println("3. Show use statistics.");
			System.out.println("4. Exit.\n");
			
			System.out.print("Selection: ");
						
			switch (userInput.nextLine()) //Selection reader.
			{	
				case "1": //Operations on numbers sub-menu and options.
					
					tries = INITIAL_TRIES; //Reset tries to 3. Used to count user's tries.
					do //While loop controls sub-menu selection and tries left.
					{		
						System.out.println("\nOperations on numbers. Select one of the three options: \n");
						System.out.println("1. Solve a standard quadratic equation. (Of the form Ax^2 + Bx + C)");
						System.out.println("2. Calculate the average of a list of numbers.");
						System.out.println("3. Return to main menu.\n");
						
						System.out.print("Selection: "); //Selection indicator.
						
						switch (userInput.nextLine()) //Selection reader.
						{
							case "1": //Standard quadratic equation solver.								
								boolean finished = false; //States if user finished used the quadratic solver.
								QuadraticEquation equationToSolve = new QuadraticEquation(); //Creates quadratic equation to solve.	
								
								do //Controls user interaction.
								{
									quadraticTaskCounter++; //Adds one to the execution counter of this task.
																		
									equationToSolve.askAndSetA(); //Gets A from user. The method prompts adequately. See method description for details.
									equationToSolve.askAndSetB(); //Gets B. 
									equationToSolve.askAndSetC(); //Gets C.
									
									//It's important to remember that the getRealSolutions method receives an QuadraticEquation type
									//object and returns an object of type FloatPair. Methods getX and getY are of the FloatPair class.
									//Hence the following instructions.
									if(equationToSolve.realSolutionsCount() == 0) //Quadratic equation has no real solutions.
									{
										System.out.println("\nThe quadratic equation specified does not have real solutions.");
									}
								
									if(equationToSolve.realSolutionsCount() == 1) //Quadratic equation has only one solution.
									{
										System.out.println("\nThe only solution is x = " + equationToSolve.getRealSolutions().getX());
									}
								
									if(equationToSolve.realSolutionsCount() == 2) //Quadratic equation has only one solution.
									{
										System.out.println("\nSolutions are x = " + equationToSolve.getRealSolutions().getX() 
															 	   + " and x = " + equationToSolve.getRealSolutions().getY());
									}
									
									System.out.print("\nPress ENTER to solve another quadratic equation.\n"
												   + "Otherwise, enter \"r\" to (r)eturn to number operations menu: ");					
										if (userInput.nextLine().equals("r")) //Prompt to return to menu or try again.
										{
											finished = true; //Checks if user selected to return to numbers menu.
										}	
									
								} while (!finished); //End while if user selected to return to number operations menu.
								
								tries = INITIAL_TRIES; //Reset tries to 3. Used to count user's tries.
								break; //End case.
								
							case "2": //List of numbers average's calculator.
								finished = false; //States if user finished using the average calculator.
								do //Controls user interaction.
								{
									averageTaskCounter++; ////Adds one to the execution counter of this task.
									double sum = 0;	//Sum of list of numbers.								
									double value = 0; //Next value to be added. Entered by the user.
									int valuesCount = 0; //Values entered
									System.out.print("\nPlease enter your values by pressing ENTER for each.\n");
									System.out.print("When finished, enter a negative value (i.e. -1): ");
									
									do //Iteration that validates and sums values supplied one by one.
									{
										try{
											value = Double.parseDouble(userInput.nextLine()); //Validates input.
											if (value >= 0)
											{
												sum += value;
												valuesCount++;
											}
										} catch (Exception valueIsNotANumber) { //Input was not a number.
											System.out.print("Your last entry wasn't a number. Please try again.");
											System.out.print("\nIf you finished, enter a negative value. (i.e. -1): ");
										}										
									} while (value >= 0); //End while if user entered a negative number (i.e. -1).
									
									if (valuesCount > 0) //Calculates and prints average with getAverage method, if any values were supplied.
									{
										System.out.print("The average of the entered values is: " + NumberList.getAverage(sum, valuesCount));
									}
									
									else //The user entered a negative number without supplying any values.
									{										
										System.out.print("\nYou didn't enter any values. You may want to try again.");
									}
									
									System.out.print("\nPress ENTER to try this operation again."
												   + "\nOtherwise, enter \"r\" to (r)eturn to number operations menu: ");							
									if (userInput.nextLine().equals("r")) //Prompt to return to menu or try again.
									{
										finished = true; //Checks if user selected to return to numbers menu.
									}	
									
								} while (!finished); //End while if user selected to return to number operations menu.
								
								tries = INITIAL_TRIES; //Reset tries to 3. Used to count user's tries.
								break; //End case.
								
							case "3": //Return to main menu.
								tries = 0;
								break; //End case.
								
							default: //Prints warning message regarding invalid selection and tries left.
								tries--;
								if (tries > 0) //The "if" comparison is to prevent printing "0" tries left.
								{
									System.out.print("\nYour selection is not valid. It must be an integer between 1 and 3. \n");
									System.out.println("Tries before the program returns to main menu: " + tries);
									System.out.print("Press ENTER to try again."); 
									userInput.nextLine(); 
								} //End if.
								
						} //End switch.	
						
					} while (tries > 0); //End while.	
					
					tries = INITIAL_TRIES; //Reset tries to 3. Used to count user's tries.
					break; //End case. (Number operations sub-menu)
					
				case "2": //Operations on strings.
					
					tries = INITIAL_TRIES; //Reset tries to 3. Used to count user's tries.
					do //While loop controls sub-menu selection and tries left.
					{		
						System.out.println("\nOperations on strings. Select one of the three options: \n");
						System.out.println("1. Reverse a given string.");
						System.out.println("2. Format a given string.");
						System.out.println("3. Return to main menu.\n");
						
						System.out.print("Selection: "); //Selection indicator.
						
						switch (userInput.nextLine()) //Selection reader.
						{
							case "1": //String reverser.								
								boolean finished = false; //States if user finished using the string reverser.
								do //Controls user interaction.
								{
									stringReverseTaskCounter++; //Adds one to the execution counter of this task.
									
									System.out.print("\nPlease enter string to reverse: "); //The reversed string is printed immediately, not stored.
									System.out.println("Reversed string: " + StringFormat.stringReverse(userInput.nextLine()));
								
									System.out.print("\nPress ENTER to reverse another string.\nOtherwise, enter \"r\" to (r)eturn to string menu: ");								
									if (userInput.nextLine().equals("r")) //Prompt to return to menu or try again.
									{
									finished = true; //Checks if user selected to return to string menu.
									}
									
								} while (!finished); //End while if user selected to return	to string menu.	
								
								tries = INITIAL_TRIES; //Reset tries to 3. Used to count user's tries.
								break; //End case.
								
							case "2": //String formatter.
								//TODO input validation. String needs to consist of "x" and "-" only! Consider new string variable instead of overwriting.								
								finished = false; //States if user finished using the string formatter.
								do //Controls user interaction.
								{
									stringFormatTaskCounter++; //Adds one to the execution counter of this task.
									
									System.out.print("\nPlease enter the string to format: ");
									String original = userInput.nextLine();
									
									System.out.print("\nPlease enter pattern. Write an \"x\" in the corresponding place where characters\n");
									System.out.println("should go and an (\"-\") to insert an hyphen in the corresponding place: ");
									String pattern = userInput.nextLine();
									
									String newString = StringFormat.patternImitator(original, pattern);
									System.out.println("\nFormatted string: " + newString);
									
									System.out.print("\nPress ENTER to format another string.\nOtherwise, enter \"r\" to (r)eturn to string menu: ");									
										if (userInput.nextLine().equals("r")) //Prompt to return to menu or try again.
										{
											finished = true; //Checks if user selected to return to string menu.
										}
										
								} while (!finished); //End while if user selected to return	to string menu.								
								
								tries = INITIAL_TRIES; //Reset tries to 3. Used to count user's tries.
								break; //End case.
								
							case "3": //Return to main menu.
								tries = 0;
								break; //End case.
								
							default: //Prints warning message regarding invalid selection and tries left.
								tries--;
								if (tries > 0) //The "if" comparison is to prevent printing "0" tries left.
								{
									System.out.print("\nYour selection is not valid. It must be an integer between 1 and 3.\n");
									System.out.println("Tries before the program returns to main menu: " + tries);
									System.out.print("Press ENTER to try again."); 
									userInput.nextLine(); 
								} //End if.
								
						} //End switch. 
						
					} while (tries > 0); //End while. 
					
					tries = INITIAL_TRIES; //Reset tries to 3. Used to count user's tries.
					break; //End case. (String operations sub-menu)
					
				case "3": //Shows use statistics. Output formatted as a table.					
					useStatisticsTaskCounter++; //Adds one to the execution counter of this task.
					
					System.out.println("\n***************************************************************************");
					System.out.println("*\t    Operation Options\t          *    Number of times executed   *");
					System.out.println("*-------------------------------------------------------------------------*");
					System.out.println("*  Solve a standard quadratic equation.   *\t\t " + quadraticTaskCounter + "\t\t  *");
					System.out.println("*     Average of a list of numbers.       *\t\t " + averageTaskCounter + "\t\t  *");
					System.out.println("*       Reverse a given string.           *\t\t " + stringReverseTaskCounter + "\t\t  *");
					System.out.println("*        Format a given string.           *\t\t " + stringFormatTaskCounter + "\t\t  *");
					System.out.println("*         Show use statistics.            *\t\t " + useStatisticsTaskCounter+ "\t\t  *");
					System.out.println("***************************************************************************");
					
					System.out.print("\nPress ENTER to return to main menu."); //Waits for user response
					userInput.nextLine(); 
					
					tries = INITIAL_TRIES; //Reset tries to 3. Used to count user's tries.
					break; //End case.
					
				case "4": //Terminate program. Prompts one last time to confirm user's decision.
					System.out.print("\nAre you sure you want to exit? Statistics data for this session will be lost!\ny=yes ENTER=no: ");
					
					if (userInput.nextLine().equals("y"))
						tries = 0; //This will terminate the program.
					else
						tries = INITIAL_TRIES; //Reset tries to 3. Used to count user's tries.
					
					break; //End case.
					
				default: //Prints warning message regarding invalid selection and tries left.
					tries--;
					if (tries > 0) //The "if" comparison is to prevent printing "0" tries left.
					{
						System.out.print("\nYour selection is not valid. It must be an integer between 1 and 4.\n");
						System.out.println("Tries before the program terminates: " + tries);
						System.out.print("Press ENTER to try again."); 
						userInput.nextLine(); 
					} //End if.
					
			}//End switch. (Main menu selection)
				
		} while (tries > 0); //End while. (Ends program)
		
		System.out.println("Program terminated."); //Termination message.
		
	}//End main.

}//End class.
