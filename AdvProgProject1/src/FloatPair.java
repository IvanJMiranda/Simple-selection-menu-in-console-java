/**
 * Objects of this type represent an ordered pair whose components x and y are type float.
 * @author Ivan J. Miranda Marrero
 *
 */
public class FloatPair 
{
	private float x; //X component.
	private float y; //Y component.
	
	/**
	 * When no argument is supplied, this default constructor pair's x and y to 0.
	 */
	public FloatPair() 
	{
		x = 0;
		y = 0;
	}
	
	/**
	 * Sets pair's x and y to the supplied parameters.
	 * @param x the x component.
	 * @param y the y component.
	 */
	public FloatPair(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Sets the pair's x component to a new value supplied by the user.
	 * @param x the new x component.
	 */
	public void setX(float x)
	{
		this.x = x;
	}
	
	/**
	 * Sets the pair's y component to a new value supplied by the user.
	 * @param y the new y component.
	 */
	public void setY(float y)
	{
		this.y = y;
	}
	
	/**
	 * Get's the pair's x component value.
	 * @return x component.
	 */
	
	
	public float getX()
	{
		return x;
	}
	
	/**
	 * Get's the pair's y component value.
	 * @return y component.
	 */
	public float getY()
	{
		return y;
	}
	
} //End class.
