import java.util.Scanner;

/**
 * Objects of this class represent a standard quadratic equation (ax^2 + bx + c) where you can modify a, b and c and a != 0.
 * @author Ivan J. Miranda Marrero
 *
 */
public class QuadraticEquation 
{
	Scanner userInput = new Scanner(System.in); //Scanner for interactive setters.
	private float a;  //Coefficient of the quadratic term.
    private float b;  //Coefficient of the linear term.
    private float c;  //Constant term.
    
    /**
     * If not initialized, the default quadratic equation will be x^2, where a = 1, b = 0 and c = 0.
     */
    public QuadraticEquation()
    {
    	this.a = 1; //Prevents dividing by zero error.
    	this.b = 0;
    	this.c = 0;
    }
    /**
     * Creates a standard quadratic equations with the given coefficients for each term.
     * @param a the coefficient of the quadratic term.
     * @param b the coefficient of the linear term.
     * @param c the constant term.
     */
    public QuadraticEquation(float a, float b, float c) 
    { 
          this.a = a; 
          this.b = b; 
          this.c = c; 
    } 
    
    /**
     * Gets the coefficient of the quadratic term as in aX^2.
     * @return a the coefficient of the quadratic term.
     */
    public float getA()
    {
    	return a;
    }	
    
    /**
     * Gets the coefficient of the linear term as in bX.
     * @return b the coefficient of the linear term
     */
    public float getB()
    {
    	return b;
    }	
    
    /**
     * Gets the constant term c.
     * @return c the constant term.
     */
    public float getC()
    {
    	return c;
    }

    /**
     * Sets the coefficient of the quadratic term as per specified by the user with the appropriate prompts and validates it.
     */
    public void askAndSetA()
    {
    	boolean aIsAValidNumber = false;
		
		do //Input validation for quadratic coefficient. a != 0.
		{
			System.out.print("\nPlease enter the coefficient of the quadratic term: ");
						
			try {
				a = Float.parseFloat(userInput.nextLine()); //Tries converting user input to float.
				if (a == 0)
					System.out.println("Error. The quadratic term's coefficient can't be zero.");
				else
					aIsAValidNumber = true; //Passed the test.
			} catch (Exception valueIsNotANumber) { 
				System.out.println("Error. You didn't enter a valid number.");
			}
		} while (a == 0 || !aIsAValidNumber); //End a validation.
    }
    
    /**
     * Sets the coefficient of the linear term as per specified by the user with the appropriate prompts and validates it.
     */
    public void askAndSetB()
    {
    	boolean bIsANumber = false;
		
		do //Input validation for linear coefficient. 
		{		
			System.out.print("Please enter the coefficient of the linear term: ");
						
			try {
				b = Float.parseFloat(userInput.nextLine()); //Tries converting user input to float.
				bIsANumber = true; //Passed the test.
			} catch(Exception valueIsNotANumber) {
				System.out.println("Error. You didn't enter a valid number.");
			}								
		} while (!bIsANumber); //End b validation.
    }
    
    /**
     * Sets the constant term as per specified by the user with the appropriate prompts and validates it.
     */
    public void askAndSetC()
    {
    	boolean cIsANumber = false;
		
		do //Input validation for constant coefficient. 
		{		
			System.out.print("Please enter the constant term: ");
						
			try {
				c = Float.parseFloat(userInput.nextLine()); //Tries converting user input to float.
				cIsANumber = true; //Passed the test.
			} catch(Exception valueIsNotANumber) {
				System.out.println("Error. You didn't enter a valid number.");
			}								
		} while (!cIsANumber); //End c validation.
    }
    
    /** Counts number of different real solutions the equation has. 
        @return An integer value in range 0..2 (0, 1, or 2)
                corresponding to the number of different real 
                solutions that the equation has. 
    */
    public int realSolutionsCount() 
    {    	
    	float discriminant = b*b-4*a*c; //The discriminant is the part inside the radical in the quadratic formula.
    	int solutions = 0; //We initially assume that the discriminant is negative, therefore, there are no real solutions.
    	
     	if (discriminant == 0) //We get one and only one solution.
    	{
    		solutions = 1;
    	}
    		
    	if (discriminant > 0) //We get two different solutions. 
    	{
    		solutions = 2;
    	}	
    	
    	return solutions;
    	
    } //End method.

    /** Returns the real solutions of the equation, if any. 
        @return Reference to a new object (a pair of Float 
                objects) if the equation has at least one real
                solution. If no real solution, it then returns
                null value. In the case of only one real solution, 
                both components of that pair reference the same
                Float object whose value is that real solutions. 
                If it has two real solutions, the components
                of that pair object are references to both, 
                respectively.
    */
    public FloatPair getRealSolutions() 
    { 
        float solution1 = (float)(-b + Math.sqrt(b*b-4*a*c))/(2*a); //Solution obtained by the SUM of -b and the sqrt of the discriminant...
        float solution2 = (float)(-b - Math.sqrt(b*b-4*a*c))/(2*a); //Solution obtained by the DIFFERENCE of -b and the sqrt of the discriminant...
        
        return new FloatPair(solution1, solution2); //Creates object of type FloarPair to store the solutions and returns its reference.
    } 
    
} //End class.

